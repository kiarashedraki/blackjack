package me.edraki;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class Player {

    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private BigDecimal totalValue;
    private String firstName;
    private String LastName;
    private Hand hand;
    private PlayerStatus status;

    public Player(BigDecimal money, String firstName, String lastName) {
        hand = new Hand();
        this.totalValue = money;
        this.firstName = firstName;
        LastName = lastName;
        this.status = PlayerStatus.ACTIVE;
    }

    public void setStatus(PlayerStatus status) {
        this.status = status;
    }

    public PlayerStatus getStatus() {
        return status;
    }

    public Hand getHand() {
        return hand;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return LastName;

    }

    public void betOnHand(BigDecimal amount) {
        hand.setBetAmount(amount);
        totalValue = totalValue.subtract(amount);
    }

    public BigDecimal getTotalValue() {
        return totalValue;
    }

    public void reset() {
        hand = new Hand();
        this.status = PlayerStatus.ACTIVE;
    }

    public void setTotalValue(BigDecimal totalValue){
        this.totalValue = totalValue;
    }
}
