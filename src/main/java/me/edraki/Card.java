package me.edraki;

public class Card {
    CardSuite suite;
    CardRank cardRank;

    public Card(CardSuite suite, CardRank cardRank) {
        this.suite = suite;
        this.cardRank = cardRank;
    }

    @Override
    public String toString() {
        return "Card{" +
                "suite=" + suite +
                ", cardRank=" + cardRank +
                '}';
    }
}