package me.edraki;


import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) throws Exception {
        BlackJack blackJack = new BlackJack();

        blackJack.buildANewDeck();

        blackJack.addPlayer(new Player(new BigDecimal("100.00"),"Kiarash","Edraki"));
        blackJack.addPlayer(new Player(new BigDecimal("100.00"),"Mahsa","Edraki"));
        blackJack.addPlayer(new Player(new BigDecimal("100.00"),"Fereydoon","Edraki"));

        blackJack.play();
    }
}
