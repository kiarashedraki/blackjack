package me.edraki;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BlackJack {

    private DeckOfCards deckOfCards;
    private Player dealer;
    private List<Player> players = new ArrayList<>();

    public void buildANewDeck() {

        deckOfCards = new DeckOfCards(8);
        deckOfCards.shuffle();
        deckOfCards.finilizeDeck();

        dealer = new Player(new BigDecimal("0"),"dealer","dealer");
    }

    public void play() throws InterruptedException {
        while (true) {

            //Reset the Players except for the Money
            for (Player player : players) {
                player.reset();
            }
            dealer.reset();

            Scanner s = new Scanner(System.in);

            for (Player player : players) {
                System.out.println("");
                System.out.println(player.getFirstName() + " please Insert The amout you are betting.");

                boolean incorrectBet = true;
                while (incorrectBet) {
                    String amountBet = s.nextLine();
                    if ((player.getTotalValue().compareTo(new BigDecimal(amountBet))) < 0){
                        System.out.println("Can't bet more than the money you have try again.");
                    }
                    else
                    {
                        incorrectBet = false;
                        player.betOnHand(new BigDecimal(amountBet));
                    }
                }
            }

            //first give each player their first two cards
            for (Player player : players) {
                player.getHand().addCard(deckOfCards.takeACard());
            }
            //dealer picks a card
            dealer.getHand().addCard(deckOfCards.takeACard());

            //second round of cards to the players
            for (Player player : players) {
                player.getHand().addCard(deckOfCards.takeACard());
            }
            // dealers second hand
            dealer.getHand().addCard(deckOfCards.takeACard());

            // Now the dealer goes around the table and asks the players if they want to hit



            for (Player player : players) {

                // While they are not busted or they say they want to stay
                while (player.getStatus().equals(PlayerStatus.ACTIVE)) {
                    System.out.println();
                    System.out.println("****************  " + player.getFirstName() + "  ******************");
                    System.out.println("Your Cards Are :");
                    for (Card card : player.getHand().getCards()) {
                        System.out.println(card.toString());
                    }
                    System.out.println("Total sum of your cards : " + player.getHand().getHandValue());
                    System.out.println("The Dealer has : " + dealer.getHand().getHandValue());
                    System.out.println(player.getFirstName() + " (H)it , (S)tay ");
                    String action = s.nextLine();
                    switch (action.toLowerCase()) {
                        case "h":
                            player.getHand().addCard(deckOfCards.takeACard());
                            if (player.getHand().getHandValue() > 21) {
                                for (Card card : player.getHand().getCards()) {
                                    System.out.println(card.toString());
                                }
                                System.out.println("Busted, your total hand value is :" + player.getHand().getHandValue());
                                player.setStatus(PlayerStatus.BUSTED);
                            }
                            break;
                        case "s":
                            player.setStatus(PlayerStatus.STAYED);
                            break;
                    }
                    System.out.println("**********************************************");
                    System.out.println();
                }
            }
            // Dealer starts hitting
            while(dealer.getHand().getHandValue() < 17){
                dealer.getHand().addCard(deckOfCards.takeACard());
                if (dealer.getHand().getHandValue() > 21){
                    dealer.setStatus(PlayerStatus.BUSTED);
                }
            }
            // Check who won
            // if the dealer is busted everyone won
            if (dealer.getStatus().equals(PlayerStatus.BUSTED)){
                // for each bet add the bet value to the total amount
                for (Player player : players){
                     player.setTotalValue(player.getTotalValue().add(player.getHand().getBetAmount()));
                }
            }
            // if the dealer didn't bust we have to check the players hands and match against the dealer
            for (Player player : players){
                if (dealer.getHand().getHandValue() > player.getHand().getHandValue()){
                    // Take don't pay the player
//                    BigDecimal lost = player.getTotalValue().subtract(player.getHand().getBetAmount());
//                    player.setTotalValue(lost);
                    //Already subtracted

                }
                if (dealer.getHand().getHandValue() < player.getHand().getHandValue()){
                    // pay the player
                    BigDecimal won = player.getTotalValue().add(player.getHand().getBetAmount().multiply(new BigDecimal(2)));
                    player.setTotalValue(won);

                }
            }

            System.out.println("The Dealer has : " + dealer.getHand().getHandValue());

            System.out.println("*********************************************");
            for(Player player : players){
                System.out.println(player.getFirstName() + " total value : " + player.getTotalValue().toString());
            }
        }
    }

    public void addPlayer(Player player) {
        players.add(player);
    }
}
