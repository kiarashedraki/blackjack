package me.edraki;

import java.util.*;

public class DeckOfCards {
    private Card[] deckOfCards;
    private Boolean finilizeDeck;
    private Queue<Card> queue;

    public DeckOfCards() {
        // build the deck of cards
        int i = 0;
        deckOfCards = new Card[52];
        for (CardSuite cardSuite : CardSuite.values()) {
            for (CardRank cardRank : CardRank.values()) {
                Card card = new Card(cardSuite, cardRank);
                deckOfCards[i] = card;
                i++;
            }
        }
    }

    public DeckOfCards(int numberOfDecks) {
        // build the deck of cards with the number of decks
        int i = 0;
        deckOfCards = new Card[52 * numberOfDecks];
        for (int j = 0; j < numberOfDecks; j++) {
            for (CardSuite cardSuite : CardSuite.values()) {
                for (CardRank cardRank : CardRank.values()) {
                    Card card = new Card(cardSuite, cardRank);
                    deckOfCards[i] = card;
                    i++;
                }
            }
        }
    }

    public void listAllCards() {
        for (int i = 0; i < deckOfCards.length; i++) {
            System.out.println(deckOfCards[i].toString());
        }
    }

    public void shuffle() {
        List<Card> cardList = Arrays.asList(deckOfCards);
        Collections.shuffle(cardList);
        deckOfCards = cardList.toArray(new Card[deckOfCards.length]);
    }


    public void finilizeDeck(){
        finilizeDeck = true;
         queue = new LinkedList<>(Arrays.asList(deckOfCards));
    }

    public Card takeACard(){
        return queue.remove();
    }

    public int numberOfCardsLeftInTheDeck(){
        return queue.size();
    }

}
