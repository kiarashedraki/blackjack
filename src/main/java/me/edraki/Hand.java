package me.edraki;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Hand {

    //logger
    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    //players hand
    private List<Card> hand;
    private BigDecimal betAmount;


    public Hand() {
        hand = new ArrayList<>();
    }

    public void addCard(Card card) {
        hand.add(card);
    }

    public int getHandValue() {
        int value = 0;
        for (Card card : hand) {
            logger.debug("The Card is :" + card.toString());
            switch (card.cardRank) {
                case ACE:
                    if (value <= 10) {
                        value += 11;
                        logger.debug("Logged :" + value);
                    } else {
                        value += 1;
                        logger.debug("Logged :" + value);
                    }
                    break;
                case TWO:
                    value += 2;
                    logger.debug("Logged :" + value);
                    break;
                case THREE:
                    value += 3;
                    logger.debug("Logged :" + value);
                    break;
                case FOUR:
                    value += 4;
                    logger.debug("Logged :" + value);
                    break;
                case FIVE:
                    value += 5;
                    logger.debug("Logged :" + value);
                    break;
                case SIX:
                    value += 6;
                    logger.debug("Logged :" + value);
                    break;
                case SEVEN:
                    value += 7;
                    logger.debug("Logged :" + value);
                    break;
                case EIGHT:
                    value += 8;
                    logger.debug("Logged :" + value);
                    break;
                case NINE:
                    value += 9;
                    logger.debug("Logged :" + value);
                    break;
                case TEN:
                    value += 10;
                    logger.debug("Logged :" + value);
                    break;
                case JACK:
                    value += 10;
                    logger.debug("Logged :" + value);
                    break;
                case QUEEN:
                    value += 10;
                    logger.debug("Logged :" + value);
                    break;
                case KING:
                    value += 10;
                    logger.debug("Logged :" + value);
                    break;
                default:
                    System.out.println("incorrect value");
                    break;
            }
        }
        return value;
    }

    public List<Card> getCards() {
        return hand;
    }

    public BigDecimal getBetAmount() {
        return betAmount;
    }

    public void setBetAmount(BigDecimal betAmount) {
        this.betAmount = betAmount;
    }


}
