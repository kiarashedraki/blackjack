package me.edraki;

public enum CardRank {
    TWO,THREE,FOUR,FIVE,SIX,SEVEN,EIGHT,NINE,TEN,
    KING,QUEEN,JACK,ACE
}
