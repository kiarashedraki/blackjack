import me.edraki.Card;
import me.edraki.DeckOfCards;
import me.edraki.Player;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class BlackJackTest {

    DeckOfCards deckOfCards;

    @Before
    public void beforeEach(){
        deckOfCards = new DeckOfCards(8);
        deckOfCards.shuffle();
        deckOfCards.finilizeDeck();
    }

    @Test
    public void testPlayer() {

        Player player = new Player(new BigDecimal(100.00),"Kiarash","Edraki");
        player.getHand().addCard(deckOfCards.takeACard());
        System.out.println(player.getHand().getHandValue());
        player.getHand().addCard(deckOfCards.takeACard());
        System.out.println(player.getHand().getHandValue());
        player.getHand().addCard(deckOfCards.takeACard());
        System.out.println(player.getHand().getHandValue());
        player.getHand().addCard(deckOfCards.takeACard());
        System.out.println(player.getHand().getHandValue());




    }
}
